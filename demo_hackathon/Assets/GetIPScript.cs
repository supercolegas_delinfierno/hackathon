﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using UnityEngine.UI;

public class GetIPScript : MonoBehaviour { 

	// Use this for initialization
	void Awake () {
        GameObject.Find("NetworkLobbyExample").GetComponent<Text>().text = LocalIPAddress() + "\n Introduzca la IP y pongase en READY" ;
	}

    private string LocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }
}
