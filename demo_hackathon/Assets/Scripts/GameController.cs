﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    //Coger alto de la pantalla, dividirlo entre los personajes (los jugadores + 20)
    public int numeroJugadores = 2;
    private List<int> posicionJugadores; // en qué lugar irán los jugadores
    private int numeroTotalPersonajes = 10;
    private float espacioVerticalEntrePj; //calcular según la cantidad de personajes y el alto de la escena
    private float margenIzquierda = -9;
    private float origenIzquierda;
    private SpriteRenderer sprite;
    private int posicionAleatoriaJugador;
    private int orden = 0;
    private ComprobarTiro comprobarTiroScript;
    private string nombrePersonajeMuerto;
    private GameObject personajeMuerto;
    private NPCManager estadoPjMuerto;
    private MoverJugador estadoJMuerto;
    private List<Sprite> imagenes;

    public GameObject npc;
    public GameObject jugador;
    public Camera mainCamara;
    public GameObject mirilla;
    public Sprite vaga1;
    public Sprite vaga2;

    public bool gameOver = false;
    
	void Awake () {

        imagenes = new List<Sprite>();
        imagenes.Add(vaga1);
        imagenes.Add(vaga2);

        comprobarTiroScript = mirilla.GetComponent<ComprobarTiro>();
        float ymax = mainCamara.orthographicSize;

        posicionJugadores = new List<int>();
        for (int i = 0; i < numeroJugadores; i++)
        {
            posicionAleatoriaJugador = Random.Range(0, numeroTotalPersonajes);
            //Debug.Log("posicion jugador: " + posicionAleatoriaJugador);
            posicionJugadores.Add(posicionAleatoriaJugador);

        }

        for (int i = 0; i < numeroTotalPersonajes; i++)
        {
            if (posicionJugadores.Contains(i))
            {
               // Debug.Log("valor de la i: " + i);
                sprite = jugador.GetComponent<SpriteRenderer>();
                sprite.sprite = imagenes[Random.Range(0,2)];
                sprite.sortingOrder = i;
                espacioVerticalEntrePj = (ymax * 2 - sprite.bounds.size.y - sprite.bounds.size.y/2) / numeroTotalPersonajes;
                jugador.name = "jugador" + i;
                origenIzquierda = margenIzquierda + Random.Range(0f, 1f);
                Instantiate(jugador, new Vector3(origenIzquierda, ymax- sprite.bounds.size.y - espacioVerticalEntrePj * i), Quaternion.identity);

            }
            else
            {
                sprite = npc.GetComponent<SpriteRenderer>();
                sprite.sprite = imagenes[Random.Range(0, 2)];
                sprite.sortingOrder = i;
                espacioVerticalEntrePj = (ymax * 2 - sprite.bounds.size.y - sprite.bounds.size.y / 2) / numeroTotalPersonajes;
                npc.name = "npc" + i;
                origenIzquierda = margenIzquierda + Random.Range(0f, 1f);
                Instantiate(npc, new Vector3(origenIzquierda, ymax - sprite.bounds.size.y - espacioVerticalEntrePj * i), Quaternion.identity);
                //Debug.Log("npc: " + i);
                
                //Debug.Log("layer: " + sprite.sortingOrder);
              

            }

        }
	}
	
	// Update is called once per frame
	void Update () {

        if(comprobarTiroScript.darEnElBlanco)
        {
           // Debug.Log("Cambiar a muerte");
            nombrePersonajeMuerto = comprobarTiroScript.personajeApuntado;
           // Debug.Log(nombrePersonajeMuerto);
            personajeMuerto = GameObject.Find(nombrePersonajeMuerto);
            if (personajeMuerto.tag == "Player")
            {
                estadoJMuerto = personajeMuerto.GetComponent<MoverJugador>();
                estadoJMuerto.muerte = true;
            }
            else
            {
                estadoPjMuerto = personajeMuerto.GetComponent<NPCManager>();
                estadoPjMuerto.MuertePersonaje();
            }
                

           
            comprobarTiroScript.darEnElBlanco = false;
        }
	
	}
}
