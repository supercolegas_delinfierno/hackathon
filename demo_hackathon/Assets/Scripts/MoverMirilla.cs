﻿using UnityEngine;
using System.Collections;

public class MoverMirilla : MonoBehaviour {

    float velocidad = 6;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if(Input.GetKey(KeyCode.RightArrow))
        {
            transform.position  += Vector3.right.normalized * velocidad * Time.deltaTime;
        }
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position -= Vector3.right.normalized * velocidad * Time.deltaTime;
        }
        if(Input.GetKey(KeyCode.DownArrow))
        {
            transform.position -= Vector3.up.normalized * velocidad * Time.deltaTime;
        }
        if(Input.GetKey(KeyCode.UpArrow))
        {
            transform.position -= Vector3.down.normalized * velocidad * Time.deltaTime;
        }
	}
}
