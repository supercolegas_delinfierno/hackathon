﻿using System;
using UnityEngine;
using Assets.Scripts.Interfaces;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.States
{
	public class EndGameState : IStateBase
	{
		private StateManager manager;
        private int sceneIndex;

        public EndGameState (StateManager managerRef)
		{
			manager = managerRef;
			Debug.Log ("Endgame state");
		}

        public void StateUpdate()
        {
            if (sceneIndex != 1) // AQUI ESTABLECER SI SE EMPIEZA OTRA PARTIDA O SE VA AL MENU
            {
                manager.SwitchState(new BeginState(manager));
                SceneManager.LoadScene(sceneIndex);
            }
        }

        public void ShowIt(){
		}

		public void StateFixedUpdate(){
		}

        public void SwtichScene(int sceneIndex)
        {
            this.sceneIndex = sceneIndex;
        }
    }
}

