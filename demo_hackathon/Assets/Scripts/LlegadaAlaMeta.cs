﻿using UnityEngine;
using System.Collections;

public class LlegadaAlaMeta : MonoBehaviour {

    GameObject gameController;
    GameController gameControllerScript;

    void Awake()
    {
        gameController = GameObject.Find("GameController");
        gameControllerScript = gameController.GetComponent<GameController>();  
        
    }

    void OnTriggerEnter2D(Collider2D pj)
    {
        Debug.Log("Cruzando meta");
        gameControllerScript.gameOver = true;
        //gameController.GetComponent<GameController>().enabled = false;
    }
}
