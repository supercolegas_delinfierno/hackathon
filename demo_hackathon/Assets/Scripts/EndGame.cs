﻿using UnityEngine;
using System.Collections.Generic;

public class EndGame : MonoBehaviour {

    Animator anim;
    GameObject gamecontroller;
    GameController gameControllerScript;
    List<GameObject> jugadores;
    int numeroJugadores;

	// Use this for initialization
	void Awake () {
        jugadores = new List<GameObject>();
       
        anim = GetComponent<Animator>();
        gamecontroller = GameObject.Find("GameController");
        gameControllerScript = gamecontroller.GetComponent<GameController>();
        numeroJugadores = gameControllerScript.numeroJugadores;
        for (int i = 0; i<numeroJugadores; i++)
        {
            jugadores.Add(GameObject.Find("jugador" + i));
        }



    }
	
	// Update is called once per frame
	void Update () {

        if (gameControllerScript.gameOver)
        {

            anim.SetTrigger("GameOver");
        }
           
	
	}
}
