﻿using UnityEngine;
using Assets.Scripts.Interfaces;
using UnityEngine.SceneManagement;
using System;

namespace Assets.Scripts.States
{
	public class BeginState : IStateBase
	{
		private StateManager manager;
        private int sceneIndex;

		public BeginState (StateManager managerRef)
		{
			manager = managerRef;
			Debug.Log ("Begin state");
		}

		public void StateUpdate(){
			if (sceneIndex != 0) {
                manager.SwitchState(new PlayState(manager));
                SceneManager.LoadScene (sceneIndex);
			}
		}

		public void ShowIt(){
		}

		public void StateFixedUpdate(){
		}

        public void SwtichScene(int sceneIndex)
        {
            this.sceneIndex = sceneIndex;
        }
    }
}

