﻿using UnityEngine;
using System.Collections;

public class MoverJugador : MonoBehaviour {

    float velocidad = 0.5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right.normalized * velocidad * Time.deltaTime;
        }

    }
}
