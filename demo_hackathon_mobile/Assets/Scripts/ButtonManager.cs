﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ButtonManager : MessageBase{

    private NetworkWriter writer;

    // Use this for initialization
    void Start()
    {
        writer = new NetworkWriter();
    }

    // Update is called once per frame
    void Update()
    {

    }

    
    public void OnAndar()
    {
        writer.StartMessage(8888);
        writer.Write("anda");
    }

    public void OnAndarReleased()
    {

    }

    public void OnCorrer()
    {

    }

    public void OnDisparar()
    {

    }

    public void OnArriba()
    {

    }

    public void OnAbajo()
    {

    }

    public void OnDerecha()
    {

    }

    public void OnIzquierda()
    {

    }

}
