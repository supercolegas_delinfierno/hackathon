﻿using UnityEngine;
using System.Collections;

public class ComprobarTiro : MonoBehaviour {

    public bool darEnElBlanco = false;
    public string personajeApuntado;

    void OnTriggerStay2D (Collider2D pj)
    { 
        if (Input.GetKeyDown(KeyCode.Space))
        {
            personajeApuntado = pj.name;
            darEnElBlanco = true;
            Debug.Log("acierta en un personaje");
        }
        else
        { 
            darEnElBlanco = false;

        }
    }
}
